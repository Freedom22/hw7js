


///--------------------------------- 3 ----------------------





function calculateAge(yearOfBirth) {

    const currentYear = new Date().getFullYear();

    const age = currentYear - yearOfBirth;

    const birthday = new Date().getTime() >= new Date(currentYear, new Date().getMonth(), new Date().getDate()).getTime();

    return birthday ? age : age - 1;
}

const yearOfBirth = prompt("Enter your birthday:");
const userAge = calculateAge(parseInt(yearOfBirth, 10));

console.log(`Ваш вік: ${userAge} років.`);