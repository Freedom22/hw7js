

///--------------------------------- 2 -------------

function length (inputString, maxLength) {
    return inputString.length <= maxLength;
}

const maxLength = 10;
const inputStr1 = "Hello";
const inputStr2 = "Hello World";

const result1 = length(inputStr1, maxLength);
const result2 = length(inputStr2, maxLength);

console.log(`${inputStr1}: ${result1}`);
console.log(`${inputStr2}: ${result2}`);