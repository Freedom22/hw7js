///-------------------------- 1 --------------


function InPalindrome(str) {
    let check = '';
    for (let i = str.length - 1; i >= 0; --i) {
      check += str[i];
    }
    return str == check;
  }
  console.log(InPalindrome('34543'));
  console.log(InPalindrome('723210'));